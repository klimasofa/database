<?php

use App\Http\Controllers\GuestHostsController;
use App\Http\Controllers\GuestVolunteersController;
use App\Http\Controllers\HostsController;
use App\Http\Controllers\KlimasofasController;
use App\Http\Controllers\VolunteersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('homepage');

Route::get('/dashboard', function () {
    return view('dashboard');
})
    ->middleware(['auth'])
    ->name('dashboard');

require __DIR__ . '/auth.php';

/*
|--------------------------------------------------------------------------
| Static pages
|--------------------------------------------------------------------------
*/

Route::view('datenschutz', 'static.datenschutz')->name('static.datenschutz');
Route::view('impressum', 'static.impressum')->name('static.impressum');
Route::view('klimasofa/danke', 'static.danke-klimasofa')->name(
    'static.danke-klimasofa',
);
Route::view('mitmachen/danke', 'static.danke-mitmachen')->name(
    'static.danke-mitmachen',
);

/*
|--------------------------------------------------------------------------
| Guest routes
|--------------------------------------------------------------------------
*/

Route::get('klimasofa', [GuestHostsController::class, 'create'])->name(
    'guesthosts.create',
);
Route::post('klimasofa', [GuestHostsController::class, 'store'])->name(
    'guesthosts.store',
);

Route::get('mitmachen', [GuestVolunteersController::class, 'create'])->name(
    'guestvolunteers.create',
);
Route::post('mitmachen', [GuestVolunteersController::class, 'store'])->name(
    'guestvolunteers.store',
);

/*
|--------------------------------------------------------------------------
| Authenticated user routes
|--------------------------------------------------------------------------
*/

Route::prefix('admin')
    ->name('admin.')
    ->middleware('auth')
    ->group(function () {
        Route::resource('hosts', HostsController::class);

        Route::resource('volunteers', VolunteersController::class);

        Route::resource('klimasofas', KlimasofasController::class);
    });
