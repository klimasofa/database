<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hosts', function (Blueprint $table) {
            $table->id();
            $table
                ->foreignId('klimasofa_id')
                ->nullable()
                ->constrained()
                ->onDelete('cascade');
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->integer('guests')->nullable();
            $table->integer('rooms')->nullable();
            $table->boolean('has_beamer')->nullable();
            $table->boolean('has_white_wall')->nullable();
            $table->string('found')->nullable();
            $table->date('date1')->nullable();
            $table->date('date2')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hosts');
    }
};
