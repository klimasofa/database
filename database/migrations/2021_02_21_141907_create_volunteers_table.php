<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteers', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table
                ->string('email')
                ->unique()
                ->nullable();
            $table->string('phone')->nullable();
            $table->string('location')->nullable();
            $table->string('education')->nullable();
            $table->string('profession')->nullable();
            $table->string('institution')->nullable();

            //            $table->text('topics')->nullable();
            //            $table->text('sessions')->nullable();
            //            $table->boolean('basics')->nullable();
            //            $table->boolean('compensation')->nullable();
            //            $table->text('notes')->nullable();

            // Internal fields
            // Probably better to have one or more "roles"
            //            $table->boolean('team')->default(false);
            //            $table->text('internal_notes')->nullable();
            //            $table->string('contact')->nullable();
            // Duzen / Siezen

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteers');
    }
};
