<?php

namespace Database\Factories;

use App\Models\Host;
use Illuminate\Database\Eloquent\Factories\Factory;

class HostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Host::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'address' => $this->faker->streetAddress,
            'email' => $this->faker->unique()->safeEmail,
            'phone' => $this->faker->phoneNumber(),
            'guests' => rand(5, 10),
            'rooms' => rand(1, 3),
            'has_beamer' => rand(0, 10) > 7,
            'has_white_wall' => rand(0, 10) > 3,
            'found' => $this->faker->sentence,
            'date1' => $this->faker
                ->dateTimeBetween('+1 week', '+4 weeks')
                ->format('Y-m-d'),
            'date2' => $this->faker
                ->dateTimeBetween('+1 week', '+4 weeks')
                ->format('Y-m-d'),
        ];
    }
}
