<?php

namespace Database\Factories;

use App\Models\Klimasofa;
use Illuminate\Database\Eloquent\Factories\Factory;

class KlimasofaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Klimasofa::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date' => $this->faker
                ->dateTimeBetween('+2 weeks', '+6 weeks')
                ->format('Y-m-d'),
        ];
    }
}
