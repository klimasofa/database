<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
    <script src="{{ asset(mix('js/app.js')) }}" defer></script>
</head>
<body class="bg-gray-200 antialiased text-gray-900">
<div class="min-h-screen max-w-screen-2xl mx-auto bg-white shadow-md lg:p-2 lg:px-6">
    <div>
        <nav class="p-1 text-klimasofa-green lg:flex lg:justify-between items-center mb-2">
            <a href="{{ url(route('homepage')) }}"
               class="flex items-center justify-center space-x-2 md:space-x-4 border-b-2 border-gray-200 pb-4 mb-4 lg:border-none lg:pb-0 lg:mb-0">
                <img src="{{ asset(mix('svg/klimasofa-logo.svg')) }}"
                     class="w-16 md:w-auto" alt="Klimasofa-Logo"/>
                <span class="font-extrabold text-2xl md:text-3xl">Klimasofa-Planung</span>
            </a>
            <div class="flex flex-row justify-center space-x-4">
                <a href="{{ url(route('guesthosts.create')) }}"
                   class="transition ease-in-out duration-300 hover:bg-klimasofa-green-dark bg-klimasofa-green text-white text-center text-sm font-semibold uppercase tracking-wide py-3 rounded-lg w-36 shadow-lg">
                    Anfragen
                </a>
                <a href="{{ url(route('guestvolunteers.create')) }}"
                   class="transition ease-in-out duration-300 hover:bg-klimasofa-green-dark bg-klimasofa-green text-white text-center text-sm font-semibold uppercase tracking-wide py-3 rounded-lg w-36 shadow-lg">
                    Mitmachen
                </a>
            </div>
        </nav>
        <div class="relative h-56 lg:h-96">
            <img class="absolute inset-0 h-full w-full object-cover object-center"
                 src="{{ asset(mix('images/forest.jpg')) }}" alt="Foto eines Regenwalds">
            <svg class="absolute bottom-0 fill-current text-white" viewBox="0 0 1482 144">
                <path d="M 0,102 C 144,49 300,38 452,58 c 122,12 238,52 358,71 230,36 469,-17 672,-129 V 144 H 0 Z"/>
            </svg>
            <h1 class="relative text-white text-center pt-20 lg:pt-36 lg:text-4xl">
                {{ $header ?? '' }}
            </h1>
        </div>
    </div>
    <div class="p-4 lg:p-1 mt-2">
        {{ $slot }}
    </div>
    <div class="p-4 lg:p-1 mt-8">
        <div class="border-t-2 border-gray-200">
            <h2 class="mt-4 text-klimasofa-green text-center">
                Gefördert aus Mitteln des #moinzukunft – Hamburger Klimafonds
            </h2>
            <div class="flex flex-col mt-4 md:flex-row justify-center items-center">
                <a href="https://moinzukunft.hamburg/klimafonds/">
                    <img src="{{ asset(mix('images/logo-hamburger-klimafonds.png')) }}" class="mb-4" alt="Logo des Hamburger Klimafonds">
                </a>
                <p class="text-sm mb-4 md:w-1/4 md:mx-6">
                    Ein Projekt der Hamburger Klimaschutzstiftung und der
                    Behörde für Umwelt, Klima, Energie und Agrarwirtschaft (BUKEA)
                </p>
                <a href="https://gut-karlshoehe.de/hamburgerklimaschutzstiftung/">
                    <img src="{{ asset(mix('images/logo-hamburger-klimaschutzstiftung.png')) }}" class="mb-4" alt="Logo der Hamburger Klimaschutzstiftung">
                </a>
            </div>
        </div>
    </div>
    <footer class="my-4 text-center text-klimasofa-green">
        <p>
            <a href="{{ url(route('static.impressum')) }}" class="mr-6">Impressum</a>
            <a href="{{ url(route('static.datenschutz')) }}">Datenschutz</a>
        </p>
    </footer>
</div>
</body>
</html>
