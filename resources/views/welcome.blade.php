<x-guest-layout>
    <x-slot name="header">
        Dein Beitrag zum Klimaschutz
    </x-slot>

    <div class="lg:flex lg:space-x-6">
        <div class="bg-klimasofa-blue w-full lg:w-1/2 rounded-lg border border-gray-200 shadow-lg p-4">
            <div class="relative flex bg-white rounded-full w-36 h-36 items-center justify-center mx-auto">
                <img class="w-20 h-20" src="{{ asset(mix('svg/klimasofa-logo.svg')) }}" alt="Klimasofa-Logo">
                <svg class="absolute inset-2 stroke-current text-gray-300 w-32 h-32" viewBox="0 0 100 100">
                    <circle r="49" cx="50" cy="50" fill="none" stroke-width="1" stroke="currentColor"/>
                </svg>
            </div>
            <h2 class="text-center mt-4">Dein Beitrag zum Klimaschutz</h2>
            <p class="mt-4">
                Du möchtest Freunde und Bekannte zu einem Klimasofa bei dir einladen?
                Dann brauchen wir von dir ein paar Informationen, damit wir mit der Planung beginnen können.
            </p>
            <div class="flex justify-center mt-6">
                <a href="{{ url(route('guesthosts.create')) }}"
                   class="transition ease-in-out duration-300 hover:bg-klimasofa-green-dark bg-klimasofa-green text-white text-center text-sm font-semibold uppercase tracking-wide py-3 rounded-lg w-36 shadow-lg">
                    Anfragen
                </a>
            </div>
        </div>
        <div class="bg-klimasofa-blue w-full lg:w-1/2 mt-6 lg:mt-0 rounded-lg border border-gray-200 shadow-lg p-4">
            <div class="relative flex bg-white rounded-full w-36 h-36 items-center justify-center mx-auto">
                <img class="w-20 h-20" src="{{ asset(mix('svg/people.svg')) }}">
                <svg class="absolute inset-2 stroke-current text-gray-300 w-32 h-32" viewBox="0 0 100 100">
                    <circle r="49" cx="50" cy="50" fill="none" stroke-width="1" stroke="currentColor"/>
                </svg>
            </div>
            <h2 class="text-center mt-4">Wir suchen dich!</h2>
            <p class="mt-4">
                Du möchtest dich für Klimaschutz und Nachhaltigkeit engagieren? Du hast auf Grund deines
                Studiums, deiner Ausbildung oder deines Berufs besonderes Wissen rund um die Themen
                Klimaschutz und Nachhaltigkeit, über die du gern einen Kurzvortrag halten möchtest?
            </p>
            <p class="mt-4">
                Du kennst dich mit einzelnen oder mehreren nachhaltigen Themen aus und hast Lust, dein
                praktisches Wissen in Kleingruppen weiterzugeben?
            </p>
            <p class="mt-4">
                Du organisierst und kommunizierst gern, bist zuverlässig, neugierig und dein Herz schlägt
                für den Klima- und Umweltschutz?
            </p>
            <p class="mt-4">
                Dann werde Teil des Klimasofas!<br>
                Wir bieten eine zeitlich flexible und wirkungsvolle Tätigkeit in einem innovativen und
                öffentlichkeitswirksamen Projekt.
            </p>
            <div class="flex justify-center mt-6">
                <a href="{{ url(route('guestvolunteers.create')) }}"
                   class="transition ease-in-out duration-300 hover:bg-klimasofa-green-dark bg-klimasofa-green text-white text-center text-sm font-semibold uppercase tracking-wide py-3 rounded-lg w-36 shadow-lg">
                    Mitmachen
                </a>
            </div>
        </div>
    </div>
</x-guest-layout>
