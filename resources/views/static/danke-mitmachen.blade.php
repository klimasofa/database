<x-guest-layout>
    <x-slot name="header">
        Vielen Dank!
    </x-slot>

    <h2 class="text-center mb-2">Toll, dass du beim Klimasofa mitmachen möchtest!</h2>

    <p class="text-center mb-2">
        Du hast eine automatische E-Mail zur
        Bestätigung von uns bekommen.
    </p>

    <p class="text-center mb-2">
        Wie geht es jetzt weiter?
    </p>

    <p class="text-center mb-2">
        Wir melden uns so schnell wie möglich bei dir,
        damit wir uns gegenseitig kennenlernen können.
    </p>

</x-guest-layout>
