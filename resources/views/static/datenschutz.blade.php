<x-guest-layout>
    <x-slot name="header">
        Datenschutz
    </x-slot>

    <h2 class="mb-2">Datenschutz</h2>

    <p>
        Wir nehmen den Schutz Deiner persönlichen Daten sehr ernst. Wir behandeln
        Deine personenbezogenen Daten vertraulich und entsprechend der gesetzlichen
        Datenschutzvorschriften sowie dieser Datenschutzerklärung.
    </p>
    <p>
        Die Nutzung dieser Website ist ohne Angabe personenbezogener Daten
        (beispielsweise ein Name, eine E-Mail-Adresse oder Telefonnummer, eine Adresse
        usw. in den Kontaktformularen) nicht möglich. Diese Daten werden nicht an
        Dritte weitergegeben.
    </p>
    <p>
        Wir weisen darauf hin, dass die Datenübertragung im Internet (beispielsweise
        bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein
        lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.
    </p>

    <h2 class="mt-8 mb-2">Cookies</h2>

    <p>
        Die Internetseiten verwenden teilweise so genannte Cookies. Cookies richten
        auf Deinem Rechner keinen Schaden an und enthalten keine Viren. Cookies dienen
        dazu, unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen.
        Cookies sind kleine Textdateien, die auf Deinem Rechner abgelegt werden und
        die Dein Browser speichert.
    </p>
    <p>
        Die meisten der von uns verwendeten Cookies sind so genannte
        „Session-Cookies“. Sie werden nach Ende Deines Besuchs automatisch gelöscht.
        Andere Cookies bleiben auf Deinem Endgerät gespeichert, bis Du diese löschst.
        Diese Cookies ermöglichen es uns, Deinen Browser beim nächsten Besuch
        wiederzuerkennen.
    </p>
    <p>
        Du kannst Deinen Browser so einstellen, dass Du über das Setzen von Cookies
        informiert wirst und Cookies nur im Einzelfall erlauben, die Annahme von
        Cookies für bestimmte Fälle oder generell ausschließen sowie das automatische
        Löschen der Cookies beim Schließen des Browser aktivieren. Bei der
        Deaktivierung von Cookies kann die Funktionalität dieser Website eingeschränkt
        sein.
    </p>

    <h2 class="mt-8 mb-2">Server-Log-Files</h2>

    <p>
        Der Provider der Seiten erhebt und speichert automatisch Informationen in so
        genannten Server-Log-Files, die Dein Browser automatisch an uns übermittelt.
        Dies sind:
    </p>
    <ul class="list-disc list-inside">
        <li>Browsertyp und -version</li>
        <li>Verwendetes Betriebssystem</li>
        <li>Referrer-URL</li>
        <li>Hostname des zugreifenden Rechners</li>
        <li>Uhrzeit der Serveranfrage</li>
    </ul>
    <p>
        IP-Adressen speichern wir nicht.
    </p>

    <h2 class="mt-8 mb-2">Anfrage- und Planungsfunktionen auf dieser Website</h2>

    <h3 class="font-bold mb-2">Gastgeberinnen und Gastgeber</h3>

    <p>
        Für die Anfragefunktion auf dieser Seite werden neben Deiner Anfrage auch
        Angaben zum Zeitpunkt der Erstellung der Anfrage gespeichert. Sämtliche
        anderen Daten, die von uns abgefragt werden, benötigen wir für die Planung des
        konkreten Klimasofa-Abends bei Dir. Wenn Du mit der Speicherung und
        Verarbeitung der Daten nicht einverstanden bist, können wir leider kein
        Klimasofa für Dich planen.
    </p>

    <h3 class="font-bold mt-2 mb-2">Expertinnen und Experten</h3>

    <p>
        Für die freiwilligen Helferinnen und Helfer sowie die Expertinnen und Experten
        werden andere Daten abgefragt, die wir für die Planung der einzelnen Abende
        benötigen. Dies sind z. B. Dein Spezialgebiet, Deine zeitliche Verfügbarkeit,
        welches Thema Dein Vortrag behandelt oder welchen Workshop Du gestalten
        kannst. Die meisten dieser Angaben sind freiwillig. Allerdings hilfst Du uns
        natürlich dabei, eine sinnvolle Planung zu machen, wenn Du viele Angaben
        machst.
    </p>

    <h2 class="mt-8 mb-2">SSL-Verschlüsselung</h2>

    <p>
        Diese Seite nutzt aus Gründen der Sicherheit und zum Schutz der Übertragung
        aller Inhalte eine SSL-Verschlüsselung. Eine verschlüsselte Verbindung
        erkennst Du daran, dass die Adresszeile des Browsers von „http://“ auf
        „https://“ wechselt und an dem Schloss-Symbol in Deiner Browserzeile.
    </p>

    <p>
        Wenn die SSL-Verschlüsselung aktiviert ist, können die Daten, die Du an uns
        übermittelst, nicht von Dritten mitgelesen werden.
    </p>

    <h2 class="mt-8 mb-2">Recht auf Auskunft, Berichtigung, Sperrung, Löschung</h2>

    <p>
        Du hast jederzeit das Recht auf unentgeltliche Auskunft über Deine
        gespeicherten personenbezogenen Daten, deren Herkunft und Empfänger und den
        Zweck der Datenverarbeitung sowie ein Recht auf Berichtigung, Sperrung oder
        Löschung dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema
        personenbezogene Daten kannst Du Dich jederzeit unter der im Impressum
        angegebenen Adresse an uns wenden.
    </p>

    <h2 class="mt-8 mb-2">Widerspruchsrecht</h2>

    <p>
        Du hast jederzeit das Recht, gegen die Verarbeitung Deiner personenbezogenen
        Daten Widerspruch einzulegen.
    </p>

    <h2 class="mt-8 mb-2">Widerspruch Werbe-Mails</h2>

    <p>
        Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten
        zur Übersendung von nicht ausdrücklich angeforderter Werbung und
        Informationsmaterialien wird hiermit widersprochen. Die Betreiber der Seiten
        behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten
        Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor.
    </p>

</x-guest-layout>
