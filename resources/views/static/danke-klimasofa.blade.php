<x-guest-layout>
    <x-slot name="header">
        Vielen Dank!
    </x-slot>

    <h2 class="text-center mb-2">Toll, dass du ein Klimasofa veranstalten möchtest!</h2>

    <p class="text-center mb-2">
        Du hast eine automatische E-Mail von uns bekommen, in der deine
        Wunschtermine aufgeführt sind.
    </p>

    <p class="text-center mb-2">
        Wie geht es jetzt weiter?
    </p>

    <p class="text-center mb-2">
        Wir melden uns so schnell wie möglich bei dir,
        um das weitere Vorgehen abzusprechen und mit der Planung anzufangen.
    </p>

</x-guest-layout>
