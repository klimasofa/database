<x-guest-layout>
    <x-slot name="header">
        Impressum
    </x-slot>

    <h2 class="mb-2">Angaben gemäß § 5 TMG</h2>

    <p>
        Fairbunden e. V.<br>
        ℅ Como Consult GmbH<br>
        Winterstraße 4–8<br>
        22765 Hamburg
    </p>

    <h2 class="mt-8 mb-2">Vertreten durch</h2>

    <p>
        Silke Quathamer, Dr. Tobias Quathamer
    </p>

    <h2 class="mt-8 mb-2">Kontakt</h2>

    <p>
        E-Mail: <a href="mailto:info@klimasofa.org">info@klimasofa.org</a><br>
        Telefon: <a href="tel:+49-40-23205054">+49 40 23205054</a>
    </p>

    <h2 class="mt-8 mb-2">Registereintrag</h2>

    <p>
        Amtsgericht Hamburg VR 24177
    </p>

    <h2 class="mt-8 mb-2">Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV</h2>

    <p>
        Fairbunden e. V.<br>
        Vertretungsberechtigt: Silke Quathamer, Dr. Tobias Quathamer<br>
        ℅ Como Consult GmbH<br>
        Winterstraße 4–8<br>
        22765 Hamburg
    </p>

    <h2 class="mt-8 mb-2">Widerspruch Werbe-Mails</h2>

    <p>
        Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten
        zur Übersendung von nicht ausdrücklich angeforderter Werbung und
        Informationsmaterialien wird hiermit widersprochen. Die Betreiber der Seiten
        behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten
        Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor.
    </p>

</x-guest-layout>
