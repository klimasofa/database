@component('mail::message')
# Bestätigung deiner Anfrage für ein Klimasofa

Hallo {{ $host->name }},

vielen Dank für deine Klimasofa-Anfrage!
Deine Wunschdaten sind:

@component('mail::panel')
    {{ \Carbon\Carbon::create($host->date1)->locale('de')->translatedFormat('l, j. F Y') }}
@endcomponent

und

@component('mail::panel')
    {{ \Carbon\Carbon::create($host->date2)->locale('de')->translatedFormat('l, j. F Y') }}
@endcomponent

Wir melden uns so schnell wie möglich bei dir,
um alles weitere abzusprechen.

Herzliche Grüße vom Klimasofa-Team
@endcomponent
