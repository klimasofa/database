@component('mail::message')
# Neue Anfrage für ein Klimasofa

Hallo Klimasofa-Team,

es gibt eine neue Klimasofa-Anfrage.

@component('mail::table')
| Frage                      | Daten                                                 |
|----------------------------|-------------------------------------------------------|
| Name                       | {{ $host->name }}                                     |
| Adresse                    | {{ $host->address }}                                  |
| E-Mail                     | <a href="mailto:{{ $host->email }}">{{ $host->email }}</a>   |
| Telefon                    | {{ $host->phone }}                                    |
| Teilnehmer*innen           | {{ $host->guests }}                                   |
| Räume                      | {{ $host->rooms }}                                    |
| Beamer vorhanden           | {{ $host->has_beamer ? "Ja" : "Nein" }}               |
| Helle Wand vorhanden       | {{ $host->has_white_wall ? "Ja" : "Nein" }}           |
| Wie aufmerksam geworden?   | {{ $host->found }}                                    |
@endcomponent

Die Wunschdaten sind:

@component('mail::panel')
    {{ \Carbon\Carbon::create($host->date1)->locale('de')->translatedFormat('l, j. F Y') }}
@endcomponent

und

@component('mail::panel')
    {{ \Carbon\Carbon::create($host->date2)->locale('de')->translatedFormat('l, j. F Y') }}
@endcomponent

{{--@component('mail::button', ['url' => route('homepage')])--}}
{{--    Anfrage ansehen--}}
{{--@endcomponent--}}
Herzliche Grüße von der Klimasofa-Planung
@endcomponent
