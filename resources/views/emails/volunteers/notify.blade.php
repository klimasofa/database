@component('mail::message')
# Neues Angebot zum Mitmachen

Hallo Klimasofa-Team,

es gibt ein neues Angebot zum Mitmachen.

@component('mail::table')
    | Frage                      | Daten                                                 |
    |----------------------------|-------------------------------------------------------|
    | Name                       | {{ $volunteer->name }}                                     |
    | E-Mail                     | <a href="mailto:{{ $volunteer->email }}">{{ $volunteer->email }}</a>   |
    | Telefon                    | {{ $volunteer->phone }}                                    |
    | Wohnort oder Stadtteil:    | {{ $volunteer->location }}                                  |
    | Basiswissen zum Klimawandel | {{ $volunteer->basics ? "Ja" : "Nein" }}               |
    | Aufwandsentschädigung      | {{ $volunteer->compensation ? "Ja" : "Nein" }}           |
    | Sonstige Mitteilungen      | {{ $volunteer->notes }}                                    |
@endcomponent

{{--@component('mail::button', ['url' => route('homepage')])--}}
{{--    Anfrage ansehen--}}
{{--@endcomponent--}}

Herzliche Grüße von der Klimasofa-Planung
@endcomponent
