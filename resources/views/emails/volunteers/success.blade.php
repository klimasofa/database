@component('mail::message')
# Danke für dein Angebot!

Hallo {{ $volunteer->name }},

vielen Dank für dein Angebot, beim Klimasofa
mitzumachen!

Wir melden uns so schnell wie möglich bei dir,
damit wir uns gegenseitig kennenlernen können.

Herzliche Grüße vom Klimasofa-Team
@endcomponent
