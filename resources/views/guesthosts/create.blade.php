<x-guest-layout>
    <x-slot name="header">
        Ein Klimasofa veranstalten
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white shadow-sm sm:rounded-lg">
                <div class="sm:p-6 bg-white">
                    <p class="mb-8">
                        Wenn du ein Klimasofa veranstalten möchtest, brauchen wir
                        von dir ein paar Daten, damit wir mit der Planung beginnen
                        können.
                    </p>

                    <x-validation-errors class="mb-8" :errors="$errors"/>

                    <form action="{{ route('guesthosts.store') }}" method="POST">
                        @csrf
                        <x-label for="name">Name:</x-label>
                        <x-input type="text" name="name" id="name" placeholder="Name" required autofocus
                                 class="mb-6 w-full" :value="old('name')"></x-input>

                        <x-label for="address">Adresse:</x-label>
                        <x-input type="text" name="address" id="address" placeholder="Adresse" required
                                 class="mb-6 w-full" :value="old('address')"></x-input>

                        <div class="sm:flex">
                            <div class="mb-6 sm:w-1/2 sm:mr-6">
                                <x-label for="email">E-Mail:</x-label>
                                <x-input type="email" name="email" id="email" placeholder="E-Mail" required
                                         class="w-full"
                                         :value="old('email')"></x-input>
                            </div>

                            <div class="mb-6 sm:w-1/2">
                                <x-label for="phone">Telefon:</x-label>
                                <x-input type="tel" name="phone" id="phone" placeholder="Telefon" required
                                         class="w-full"
                                         :value="old('phone')"></x-input>
                            </div>
                        </div>

                        <div class="sm:flex">
                            <div class="mb-6 sm:w-1/2 sm:mr-6">
                                <x-label for="guests">Anzahl der Teilnehmer*innen:</x-label>
                                <x-input type="number" name="guests" id="guests" placeholder="Anzahl" required
                                         class="w-full"
                                         :value="old('guests')"></x-input>
                            </div>

                            <div class="mb-6 sm:w-1/2">
                                <x-label for="rooms">Anzahl der Räume:</x-label>
                                <x-input type="number" name="rooms" id="rooms" placeholder="Räume" required
                                         class="w-full"
                                         :value="old('rooms')"></x-input>
                            </div>
                        </div>

                        <div class="sm:flex">
                            <div class="mb-6 sm:w-1/2 sm:mr-6">
                                <x-label for="date1">Wunschdatum 1:</x-label>
                                <x-input type="date" name="date1" id="date1" required class="w-full"
                                         :value="old('date1')"></x-input>
                            </div>

                            <div class="mb-6 sm:w-1/2">
                                <x-label for="date2">Wunschdatum 2:</x-label>
                                <x-input type="date" name="date2" id="date2" required class="w-full"
                                         :value="old('date2')"></x-input>
                            </div>
                        </div>

                        <div class="sm:flex">
                            <div class="flex items-center mb-6 sm:w-1/2 sm:mr-6">
                                <x-label for="has_beamer" class="mr-2">Ist ein Beamer vorhanden?</x-label>
                                <select name="has_beamer" id="has_beamer"
                                        class="rounded-md shadow-sm border-gray-300 focus:border-blue-500 focus:ring focus:ring-blue-500 focus:ring-opacity-50">
                                    <option></option>
                                    <option value="1" @if (old('has_beamer') === '1') {{ 'selected' }} @endif>Ja
                                    </option>
                                    <option value="0" @if (old('has_beamer') === '0') {{ 'selected' }} @endif>Nein
                                    </option>
                                </select>
                            </div>

                            <div class="flex items-center mb-6 sm:w-1/2">
                                <x-label for="has_white_wall" class="mr-2">Ist eine weiße oder helle Wand für den Beamer
                                    vorhanden?
                                </x-label>
                                <select name="has_white_wall" id="has_white_wall"
                                        class="rounded-md shadow-sm border-gray-300 focus:border-blue-500 focus:ring focus:ring-blue-500 focus:ring-opacity-50">
                                    <option></option>
                                    <option value="1" @if (old('has_white_wall') === '1') {{ 'selected' }} @endif>Ja
                                    </option>
                                    <option value="0" @if (old('has_white_wall') === '0') {{ 'selected' }} @endif>Nein
                                    </option>
                                </select>
                            </div>
                        </div>

                        <x-label for="found">Wie oder wodurch bist du auf uns aufmerksam geworden?</x-label>
                        <x-input type="text" name="found" id="found"
                                 class="mb-6 w-full" :value="old('found')"></x-input>

                        <div class="flex items-center mb-6">
                            <x-input type="checkbox" name="privacy" id="privacy" class="mr-3"></x-input>
                            <x-label for="privacy">Ich habe die <a href="{{ route('static.datenschutz') }}" class="text-klimasofa-red hover:underline">Datenschutzerklärung</a>
                                gelesen und willige in die Speicherung und Verarbeitung meiner Daten ein.
                            </x-label>
                        </div>

                        <div class="flex justify-center">
                            <input type="submit" value="Abschicken"
                                   class="inline-flex px-6 py-3 transition ease-in-out duration-300 hover:bg-klimasofa-green-dark focus:border-blue-500 focus:outline-none focus:ring focus:ring-blue-500 focus:ring-opacity-50 bg-klimasofa-green text-white text-center text-sm font-semibold uppercase tracking-wide rounded-lg shadow-lg mr-3">

                            <a href="{{ route('homepage') }}" role="button"
                               class="inline-flex px-6 py-3 transition ease-in-out duration-300 hover:bg-gray-400 focus:border-blue-500 focus:outline-none focus:ring focus:ring-blue-500 focus:ring-opacity-50 bg-gray-300 text-black text-center text-sm font-semibold uppercase tracking-wide rounded-lg shadow-lg">
                                Abbrechen
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
