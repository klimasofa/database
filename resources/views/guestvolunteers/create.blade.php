<x-guest-layout>
    <x-slot name="header">
        Beim Klimasofa mitmachen
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white shadow-sm sm:rounded-lg">
                <div class="sm:p-6 bg-white">
                    <p class="mb-8">
                        (Fast) alle Angaben sind freiwillig, es gibt nur
                        zwei Pflichtfelder. Dies sind dein Name und deine
                        E-Mail-Adresse. Wir freuen uns aber, wenn du möglichst
                        viele Angaben machst. Das erleichtert uns die
                        Planung der Klimasofas.
                    </p>

                    <x-validation-errors class="mb-8" :errors="$errors"/>

                    <form action="{{ route('guestvolunteers.store') }}" method="POST">
                        @csrf
                        <x-label for="name">Name:</x-label>
                        <x-input type="text" name="name" id="name" placeholder="Name" required autofocus
                                 class="mb-6 w-full" :value="old('name')"></x-input>

                        <div class="sm:flex">
                            <div class="mb-6 sm:w-1/2 sm:mr-6">
                                <x-label for="email">E-Mail:</x-label>
                                <x-input type="email" name="email" id="email" placeholder="E-Mail" required
                                         class="w-full"
                                         :value="old('email')"></x-input>
                            </div>

                            <div class="mb-6 sm:w-1/2">
                                <x-label for="phone">Telefon:</x-label>
                                <x-input type="tel" name="phone" id="phone" placeholder="Telefon" required
                                         class="w-full"
                                         :value="old('phone')"></x-input>
                            </div>
                        </div>

                        <x-label for="address">Wohnort oder Stadtteil:</x-label>
                        <x-input type="text" name="location" id="location" placeholder="Wohnort oder Stadtteil" required
                                 class="mb-6 w-full" :value="old('location')"></x-input>

                        <div class="sm:flex">
                            <div class="flex items-center mb-6 sm:w-1/2 sm:mr-6">
                                <x-label for="basics" class="mr-2">Basiswissen zum Klimawandel kann vermittelt werden:</x-label>
                                <select name="basics" id="basics"
                                        class="rounded-md shadow-sm border-gray-300 focus:border-blue-500 focus:ring focus:ring-blue-500 focus:ring-opacity-50">
                                    <option></option>
                                    <option value="1" @if (old('basics') === '1') {{ 'selected' }} @endif>Ja
                                    </option>
                                    <option value="0" @if (old('basics') === '0') {{ 'selected' }} @endif>Nein
                                    </option>
                                </select>
                            </div>

                            <div class="flex items-center mb-6 sm:w-1/2">
                                <x-label for="compensation" class="mr-2">Aufwandsentschädigung erwünscht:</x-label>
                                <select name="compensation" id="compensation"
                                        class="rounded-md shadow-sm border-gray-300 focus:border-blue-500 focus:ring focus:ring-blue-500 focus:ring-opacity-50">
                                    <option></option>
                                    <option value="1" @if (old('compensation') === '1') {{ 'selected' }} @endif>Ja
                                    </option>
                                    <option value="0" @if (old('compensation') === '0') {{ 'selected' }} @endif>Nein
                                    </option>
                                </select>
                            </div>
                        </div>

                        <x-label for="notes">Möchtest du uns noch etwas mitteilen?</x-label>
                        <x-textarea name="notes" id="notes"
                                 class="mb-6 w-full">{{ old('notes') }}</x-textarea>

                        <div class="flex items-center mb-6">
                            <x-input type="checkbox" name="privacy" id="privacy" class="mr-3"></x-input>
                            <x-label for="privacy">Ich habe die <a href="{{ route('static.datenschutz') }}" class="text-klimasofa-red hover:underline">Datenschutzerklärung</a>
                                gelesen und willige in die Speicherung und Verarbeitung meiner Daten ein.
                            </x-label>
                        </div>

                        <div class="flex justify-center">
                            <input type="submit" value="Abschicken"
                                   class="inline-flex px-6 py-3 transition ease-in-out duration-300 hover:bg-klimasofa-green-dark focus:border-blue-500 focus:outline-none focus:ring focus:ring-blue-500 focus:ring-opacity-50 bg-klimasofa-green text-white text-center text-sm font-semibold uppercase tracking-wide rounded-lg shadow-lg mr-3">

                            <a href="{{ route('homepage') }}" role="button"
                               class="inline-flex px-6 py-3 transition ease-in-out duration-300 hover:bg-gray-400 focus:border-blue-500 focus:outline-none focus:ring focus:ring-blue-500 focus:ring-opacity-50 bg-gray-300 text-black text-center text-sm font-semibold uppercase tracking-wide rounded-lg shadow-lg">
                                Abbrechen
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
