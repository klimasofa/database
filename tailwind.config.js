const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    content: [
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                'klimasofa-green': {
                    // light: '#85d7ff',
                    DEFAULT: '#137b5a',
                    dark: '#11634a',
                    darker: '#0c4f3a',
                },
                'klimasofa-red': '#96234e',
                'klimasofa-blue': '#e6f4f7',
                'klimasofa-yellow': '#f9a620',
            },
        },
    },

    plugins: [require('@tailwindcss/forms')],
};
