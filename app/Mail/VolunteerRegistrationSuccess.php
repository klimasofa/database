<?php

namespace App\Mail;

use App\Models\Volunteer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VolunteerRegistrationSuccess extends Mailable
{
    use Queueable, SerializesModels;

    public Volunteer $volunteer;

    /**
     * Create a new message instance.
     */
    public function __construct(Volunteer $volunteer)
    {
        $this->volunteer = $volunteer;
    }

    /**
     * Build the message.
     */
    public function build(): VolunteerRegistrationSuccess
    {
        return $this->subject('Vielen Dank für deine Anmeldung')->markdown(
            'emails.volunteers.success',
        );
    }
}
