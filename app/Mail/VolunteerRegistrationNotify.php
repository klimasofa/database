<?php

namespace App\Mail;

use App\Models\Volunteer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VolunteerRegistrationNotify extends Mailable
{
    use Queueable, SerializesModels;

    public Volunteer $volunteer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Volunteer $volunteer)
    {
        $this->volunteer = $volunteer;
    }

    /**
     * Build the message.
     */
    public function build(): VolunteerRegistrationNotify
    {
        return $this->subject('Angebot zum Mitmachen')->markdown(
            'emails.volunteers.notify',
        );
    }
}
