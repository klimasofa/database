<?php

namespace App\Mail;

use App\Models\Host;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class HostRegistrationNotify extends Mailable
{
    use Queueable, SerializesModels;

    public Host $host;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Host $host)
    {
        $this->host = $host;
    }

    /**
     * Build the message.
     */
    public function build(): HostRegistrationNotify
    {
        return $this->subject('Neue Klimasofa-Anfrage')->markdown(
            'emails.hosts.notify',
        );
    }
}
