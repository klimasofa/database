<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Klimasofa extends Model
{
    use HasFactory;

    protected $fillable = ['date', 'status'];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'status' => 'new',
    ];

    public function host(): HasOne
    {
        return $this->hasOne(Host::class);
    }
}
