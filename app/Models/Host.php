<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Host extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'address',
        'email',
        'phone',
        'guests',
        'rooms',
        'has_beamer',
        'has_white_wall',
        'found',
        'date1',
        'date2',
    ];

    /**
     * The attributes that should be cast.
     */
    protected $casts = [
        'guests' => 'integer',
        'rooms' => 'integer',
        'has_beamer' => 'boolean',
        'has_white_wall' => 'boolean',
    ];

    public function klimasofa(): BelongsTo
    {
        return $this->belongsTo(Klimasofa::class);
    }
}
