<?php

namespace App\Http\Controllers;

use App\Mail\VolunteerRegistrationNotify;
use App\Mail\VolunteerRegistrationSuccess;
use App\Models\Volunteer;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class GuestVolunteersController extends Controller
{
    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('guestvolunteers.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $attributes = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'nullable',
            'location' => 'nullable',
            'education' => 'nullable',
            'profession' => 'nullable',
            'institution' => 'nullable',
            'privacy' => 'accepted',
        ]);

        $volunteer = Volunteer::create($attributes);

        $mailable = new VolunteerRegistrationSuccess($volunteer);
        Mail::to($volunteer)->send($mailable);

        // @todo: include the url to the volunteer for the team
        $mailable = new VolunteerRegistrationNotify($volunteer);
        Mail::to(env('MAIL_FROM_ADDRESS'))->send($mailable);

        return redirect()->route('static.danke-mitmachen');
    }
}
