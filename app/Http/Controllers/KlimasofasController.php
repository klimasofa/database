<?php

namespace App\Http\Controllers;

use App\Models\Klimasofa;
use Illuminate\Http\Request;

class KlimasofasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // @todo validate
        $attributes = $request->all();
        Klimasofa::create($attributes);
        return redirect()->route('admin.klimasofas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Klimasofa  $klimasofa
     * @return \Illuminate\Http\Response
     */
    public function show(Klimasofa $klimasofa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Klimasofa  $klimasofa
     * @return \Illuminate\Http\Response
     */
    public function edit(Klimasofa $klimasofa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Klimasofa  $klimasofa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Klimasofa $klimasofa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Klimasofa  $klimasofa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Klimasofa $klimasofa)
    {
        //
    }
}
