<?php

namespace App\Http\Controllers;

use App\Models\Host;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class HostsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $hosts = Host::all();
        return view('hosts.index', compact('hosts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $attributes = $request->validate([
            'name' => 'required',
            'address' => 'nullable',
            'email' => 'required|email',
            'phone' => 'nullable',
            'guests' => 'nullable|integer',
            'rooms' => 'nullable|integer',
            'has_beamer' => 'nullable|boolean',
            'has_white_wall' => 'nullable|boolean',
            'found' => 'nullable',
            'date1' => 'nullable|date',
            'date2' => 'nullable|date',
        ]);

        Host::create($attributes);

        // @todo: show a flash success
        return redirect()->route('homepage');
    }

    /**
     * Display the specified resource.
     */
    public function show(Host $host): View
    {
        return view('hosts.show', compact('host'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Host $host)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Host $host)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Host $host)
    {
        //
    }
}
