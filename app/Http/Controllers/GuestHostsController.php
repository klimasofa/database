<?php

namespace App\Http\Controllers;

use App\Mail\HostRegistrationNotify;
use App\Mail\HostRegistrationSuccess;
use App\Models\Host;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class GuestHostsController extends Controller
{
    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('guesthosts.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $attributes = $request->validate([
            'name' => 'required',
            'address' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'guests' => 'required|integer',
            'rooms' => 'required|integer',
            'has_beamer' => 'required|boolean',
            'has_white_wall' => 'required|boolean',
            'found' => 'nullable',
            'date1' => 'required|date|after:+6 days',
            'date2' => 'required|date|after:+6 days',
            'privacy' => 'accepted',
        ]);

        $host = Host::create($attributes);

        $mailable = new HostRegistrationSuccess($host);
        Mail::to($host)->send($mailable);

        // @todo: include the url to the host for the team
        $mailable = new HostRegistrationNotify($host);
        Mail::to(env('MAIL_FROM_ADDRESS'))->send($mailable);

        return redirect()->route('static.danke-klimasofa');
    }
}
