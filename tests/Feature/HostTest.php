<?php

namespace Tests\Feature;

use App\Models\Host;
use App\Models\User;
use Tests\TestCase;

class HostTest extends TestCase
{
    /** @test */
    public function a_guest_cannot_view_update_or_destroy_a_host()
    {
        $this->get(route('admin.hosts.index'))->assertRedirect('login');
        $this->get(route('admin.hosts.edit', 1))->assertRedirect('login');
        $this->get(route('admin.hosts.show', 1))->assertRedirect('login');
        $this->delete(route('admin.hosts.destroy', 1))->assertRedirect('login');
        $this->put(route('admin.hosts.update', 1))->assertRedirect('login');
    }

    /** @test */
    public function a_user_can_create_a_new_host()
    {
        $this->actingAs(User::factory()->create());
        $host = Host::factory()->raw();

        $this->assertDatabaseMissing('hosts', $host);

        $this->get(route('admin.hosts.create'))->assertOk();

        $this->post(route('admin.hosts.store'), $host)->assertRedirect(
            route('homepage'),
        );

        $this->assertDatabaseHas('hosts', $host);
    }

    /** @test */
    public function a_user_can_view_update_or_destroy_a_host()
    {
        $this->actingAs(User::factory()->create());

        $host = Host::factory()->create();

        $this->get(route('admin.hosts.index'))->assertOk();
        $this->get(route('admin.hosts.edit', $host))->assertOk();
        $this->get(route('admin.hosts.show', $host))->assertOk();
        $this->put(route('admin.hosts.update', $host))->assertOk();
        $this->delete(route('admin.hosts.destroy', $host))->assertOk();
    }

    /** @test */
    public function a_host_needs_at_least_a_name_and_email()
    {
        $this->actingAs(User::factory()->create());

        $host = Host::factory()->raw(['name' => '']);

        $this->get(route('admin.hosts.create'))->assertOk();
        $this->post(route('admin.hosts.store'), $host)->assertSessionHasErrors(
            'name',
        );
        $this->assertDatabaseMissing('hosts', $host);

        $host = Host::factory()->raw(['email' => '']);

        $this->get(route('admin.hosts.create'))->assertOk();
        $this->post(route('admin.hosts.store'), $host)->assertSessionHasErrors(
            'email',
        );
        $this->assertDatabaseMissing('hosts', $host);
    }

    /** @test */
    public function a_new_host_is_listed_on_the_index_page()
    {
        $this->actingAs(User::factory()->create());

        // Make a host, but don't persist it to the DB yet.
        $host = Host::factory()->make();
        $this->get(route('admin.hosts.index'))->assertDontSeeText($host->name);

        $host->save();
        $this->get(route('admin.hosts.index'))->assertSeeText($host->name);
    }

    /** @test */
    public function a_user_can_view_a_host()
    {
        $this->actingAs(User::factory()->create());
        $host = Host::factory()->create();

        $this->get(route('admin.hosts.show', $host->id))->assertSeeText(
            $host->name,
        );
    }
}
