<?php

namespace Tests\Feature;

use App\Mail\HostRegistrationNotify;
use App\Mail\HostRegistrationSuccess;
use App\Mail\VolunteerRegistrationNotify;
use App\Mail\VolunteerRegistrationSuccess;
use App\Models\Host;
use App\Models\User;
use App\Models\Volunteer;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class GuestVolunteerTest extends TestCase
{
    /** @test */
    public function a_guest_can_view_the_volunteer_creation_page()
    {
        $this->get(route('guestvolunteers.create'))->assertSeeText('Name:');
    }

    /** @test */
    public function a_guest_can_create_a_new_volunteer()
    {
        $volunteer = Volunteer::factory()->raw();

        $this->get(route('guestvolunteers.create'))->assertOk();

        // Accept the privacy field
        $volunteer['privacy'] = true;

        $this->post(route('guestvolunteers.store'), $volunteer)->assertRedirect(
            route('static.danke-mitmachen'),
        );

        // Remove privacy again, because it's not stored in the DB
        unset($volunteer['privacy']);

        $this->assertDatabaseHas('volunteers', $volunteer);
    }

    /** @test */
    public function a_mail_is_sent_to_the_guest_after_registration()
    {
        Mail::fake();

        $volunteer = Volunteer::factory()->raw();

        // Accept the privacy field
        $volunteer['privacy'] = true;

        $this->post(route('guestvolunteers.store'), $volunteer)->assertRedirect(
            route('static.danke-mitmachen'),
        );

        Mail::assertSent(VolunteerRegistrationSuccess::class);
        Mail::assertSent(VolunteerRegistrationNotify::class);
    }
}
