<?php

namespace Tests\Feature;

use App\Models\Host;
use App\Models\Klimasofa;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class KlimasofaTest extends TestCase
{
    /** @test */
    public function a_guest_cannot_access_klimasofas()
    {
        $klimasofa = Klimasofa::factory()->raw();

        $this->get(route('admin.klimasofas.index'))->assertRedirect('login');

        $this->get(route('admin.klimasofas.create'))->assertRedirect(
            route('login'),
        );
        $this->post(
            route('admin.klimasofas.store'),
            $klimasofa,
        )->assertRedirect(route('login'));
        $this->assertDatabaseMissing('klimasofas', $klimasofa);

        $this->get(route('admin.klimasofas.edit', 1))->assertRedirect('login');
        $this->get(route('admin.klimasofas.show', 1))->assertRedirect('login');
        $this->delete(route('admin.klimasofas.destroy', 1))->assertRedirect(
            'login',
        );
        $this->put(route('admin.klimasofas.update', 1))->assertRedirect(
            'login',
        );
    }

    /** @test */
    public function a_user_can_create_a_klimasofa()
    {
        $user = User::factory()->create();
        $klimasofa = Klimasofa::factory()->raw();

        $this->actingAs($user);

        $this->get(route('admin.klimasofas.create'))->assertOk();
        $this->post(
            route('admin.klimasofas.store'),
            $klimasofa,
        )->assertRedirect(route('admin.klimasofas.index'));
        $this->assertDatabaseHas('klimasofas', $klimasofa);
    }

    /** @test */
    public function it_checks_that_a_klimasofa_can_have_a_host()
    {
        $host = Host::create();
        $klimasofa = Klimasofa::create();

        $this->assertNull($klimasofa->host);

        $klimasofa->host = $host;

        $this->assertEquals($host, $klimasofa->host);
    }

    /** @test */
    public function it_checks_that_a_klimasofa_can_have_a_date()
    {
        $date = Carbon::now();
        $klimasofa = Klimasofa::create();

        $this->assertNull($klimasofa->date);

        $klimasofa->date = $date;

        $this->assertEquals($date, $klimasofa->date);
    }

    /** @test */
    public function it_checks_that_a_new_klimasofa_has_the_correct_status()
    {
        $klimasofa = Klimasofa::create();

        $this->assertEquals('new', $klimasofa->status);
    }
}
