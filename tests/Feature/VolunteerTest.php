<?php

namespace Tests\Feature;

use App\Mail\HostRegistrationNotify;
use App\Mail\HostRegistrationSuccess;
use App\Mail\VolunteerRegistrationNotify;
use App\Mail\VolunteerRegistrationSuccess;
use App\Models\Host;
use App\Models\User;
use App\Models\Volunteer;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class VolunteerTest extends TestCase
{
    /** @test */
    public function a_guest_cannot_view_update_or_destroy_a_volunteer()
    {
        $this->get(route('admin.volunteers.index'))->assertRedirect('login');
        $this->get(route('admin.volunteers.edit', 1))->assertRedirect('login');
        $this->get(route('admin.volunteers.show', 1))->assertRedirect('login');
        $this->delete(route('admin.volunteers.destroy', 1))->assertRedirect(
            'login',
        );
        $this->put(route('admin.volunteers.update', 1))->assertRedirect(
            'login',
        );
    }

    /** @test */
    public function a_user_can_create_a_new_volunteer()
    {
        $this->actingAs(User::factory()->create());

        $volunteer = Volunteer::factory()->raw();

        $this->get(route('admin.volunteers.create'))->assertOk();

        // Accept the privacy field
        $volunteer['privacy'] = true;

        $this->post(
            route('admin.volunteers.store'),
            $volunteer,
        )->assertRedirect(route('homepage'));

        // Remove privacy again, because it's not stored in the DB
        unset($volunteer['privacy']);

        $this->assertDatabaseHas('volunteers', $volunteer);
    }

    /** @test */
    public function a_user_can_view_update_or_destroy_a_volunteer()
    {
        $this->actingAs(User::factory()->create());

        Volunteer::factory()->create();

        $this->get(route('admin.volunteers.index'))->assertOk();
        $this->get(route('admin.volunteers.edit', 1))->assertOk();
        $this->get(route('admin.volunteers.show', 1))->assertOk();
        $this->put(route('admin.volunteers.update', 1))->assertOk();
        $this->delete(route('admin.volunteers.destroy', 1))->assertOk();
    }

    /** @test */
    public function a_volunteer_needs_a_name_and_email()
    {
        $this->actingAs(User::factory()->create());

        $volunteer = Volunteer::factory()->raw(['name' => '']);

        $this->get(route('admin.volunteers.create'))->assertOk();

        $this->post(
            route('admin.volunteers.store'),
            $volunteer,
        )->assertSessionHasErrors('name');
        $this->assertDatabaseMissing('volunteers', $volunteer);

        $volunteer = Volunteer::factory()->raw(['email' => '']);

        $this->get(route('admin.volunteers.create'))->assertOk();
        $this->post(
            route('admin.volunteers.store'),
            $volunteer,
        )->assertSessionHasErrors('email');
        $this->assertDatabaseMissing('volunteers', $volunteer);
    }

    /** @test */
    public function a_volunteer_is_listed_on_the_index_page()
    {
        $this->actingAs(User::factory()->create());

        // Make a volunteer, but don't persist it to the DB yet.
        $volunteer = Volunteer::factory()->make();
        $this->get(route('admin.volunteers.index'))->assertDontSeeText(
            $volunteer->name,
        );

        $volunteer->save();
        $this->get(route('admin.volunteers.index'))->assertSeeText(
            $volunteer->name,
        );
    }
}
