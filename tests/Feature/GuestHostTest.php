<?php

namespace Tests\Feature;

use App\Mail\HostRegistrationNotify;
use App\Mail\HostRegistrationSuccess;
use App\Models\Host;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class GuestHostTest extends TestCase
{
    /** @test */
    public function a_guest_can_view_the_host_creation_page()
    {
        $this->get(route('guesthosts.create'))->assertSeeText('Name:');
    }

    /** @test */
    public function a_guest_can_create_a_new_host()
    {
        $host = Host::factory()->raw();

        $this->assertDatabaseMissing('hosts', $host);

        $this->get(route('guesthosts.create'))->assertOk();

        // Accept the privacy field
        $host['privacy'] = true;

        $this->post(route('guesthosts.store'), $host)->assertRedirect(
            route('static.danke-klimasofa'),
        );

        // Remove privacy again, because it's not stored in the DB
        unset($host['privacy']);

        $this->assertDatabaseHas('hosts', $host);
    }

    /** @test */
    public function a_host_needs_all_data()
    {
        $required_attributes = [
            'name',
            'address',
            'email',
            'phone',
            'guests',
            'rooms',
            'date1',
            'date2',
        ];

        foreach ($required_attributes as $required_attribute) {
            $host = Host::factory()->raw([$required_attribute => '']);

            $this->get(route('guesthosts.create'))->assertOk();
            $this->post(
                route('guesthosts.store'),
                $host,
            )->assertSessionHasErrors($required_attribute);
            $this->assertDatabaseMissing('hosts', $host);
        }
    }

    /** @test */
    public function dates_must_be_some_days_in_the_future()
    {
        $host = Host::factory()->raw([
            'date1' => date('Y-m-d', time() + 3 * 86400),
        ]);

        $this->get(route('guesthosts.create'))->assertOk();
        $this->post(route('guesthosts.store'), $host)->assertSessionHasErrors(
            'date1',
        );
        $this->assertDatabaseMissing('hosts', $host);

        $host = Host::factory()->raw([
            'date2' => date('Y-m-d', time() + 3 * 86400),
        ]);

        $this->get(route('guesthosts.create'))->assertOk();
        $this->post(route('guesthosts.store'), $host)->assertSessionHasErrors(
            'date2',
        );
        $this->assertDatabaseMissing('hosts', $host);
    }

    /** @test */
    public function a_mail_is_sent_to_the_guest_after_registration()
    {
        Mail::fake();

        $host = Host::factory()->raw();
        // Accept the privacy field
        $host['privacy'] = true;

        $this->post(route('guesthosts.store'), $host)->assertRedirect(
            route('static.danke-klimasofa'),
        );

        Mail::assertSent(HostRegistrationSuccess::class);
        Mail::assertSent(HostRegistrationNotify::class);
    }
}
